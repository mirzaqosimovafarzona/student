package uz.pdp.platforma10_2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Platforma102Application {

    public static void main(String[] args) {
        SpringApplication.run(Platforma102Application.class, args);
    }

}
