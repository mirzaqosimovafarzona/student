package uz.pdp.platforma10_2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.platforma10_2.entity.University;

@Repository
public interface UniversityRepository extends JpaRepository<University, Long> {

}
