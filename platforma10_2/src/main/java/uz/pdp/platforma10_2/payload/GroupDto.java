package uz.pdp.platforma10_2.payload;

import lombok.Data;

@Data
public class GroupDto {

    private String name;
    private Long facultyId;

}
